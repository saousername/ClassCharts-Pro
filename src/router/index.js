import { createRouter, createWebHistory } from "vue-router"

import Behaviour from '@/views/Behaviour'
import Homeworks from '@/views/Homeworks'
import Detentions from '@/views/Detentions'

const routes = [
    {
        path: '/behaviour',
        name: 'Behaviour',
        alias: '/',
        component: Behaviour
    },
    {
      path: '/homeworks',
      name: 'Homeworks',
      component: Homeworks
  },
  {
    path: '/detentions',
    name: 'Detentions',
    component: Detentions
},
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

export default router